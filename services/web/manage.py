from flask.cli import FlaskGroup
from project import app
import psycopg2

cli = FlaskGroup(app)

if __name__ == "__main__":
  cli()
